

var idBtnEnviar = document.getElementById("idBtnEnviar")

idBtnEnviar.onclick = async function () {
   
    let respostaSolicitacao = document.getElementById("idSolicitacao").value
    let respostaNotaAtendimento = document.getElementById("idNotaAtendimento").value
    let respostaNotaAtendente = document.getElementById("idNotaAtendente").value
    let respostaNotaProduto = document.getElementById("idRangeProduto").value
    let respostaIdadeEntrevistado = document.getElementById("idIdadeEntrevistado").value
    var respostaGeneroEntrevistado = document.querySelector("input[name=nmRadioGenero]:checked").value


    let dados = {
        respostaSolicitacao: respostaSolicitacao,
        respostaNotaAtendimento: respostaNotaAtendimento,
        respostaNotaAtendente: respostaNotaAtendente,
        respostaNotaProduto: respostaNotaProduto,
        respostaIdadeEntrevistado: respostaIdadeEntrevistado,
        respostaGeneroEntrevistado: respostaGeneroEntrevistado
    }

    let registraResposta = await registrarResposta(dados)
    let mensagemResposta =  document.getElementById("mensagemResposta")

    if (registraResposta.status == "500") {
        mensagemResposta.className = "text-danger fs-4 mt-2"
        mensagemResposta.innerText = "Erro ao enviar resposta"
        limpaFormulario()
        setTimeout(() => {
            mensagemResposta.className = ""
        mensagemResposta.innerText = ""
        }, 2000);
    } else{
        mensagemResposta.innerText = "Pesquisa Registrada Com sucesso"
        mensagemResposta.className = "text-success fs-4 mt-2"
        limpaFormulario()
        setTimeout(() => {
            mensagemResposta.className = ""
        mensagemResposta.innerText = ""
        }, 2000);
    }

    console.log(registraResposta)
}

async function registrarResposta(dados) {
    var options = { 
        method: "POST", 
        body: JSON.stringify(dados), 
        headers: {"Content-Type":"application/json" }
   }

    let response = await fetch("http://localhost:3001/pesquisa",options)


    return response.json()
}

function limpaFormulario() {
    let respostaSolicitacao = document.getElementById("idSolicitacao").value = "" 
    let respostaNotaAtendimento = document.getElementById("idNotaAtendimento").value = ""
    let respostaNotaAtendente = document.getElementById("idNotaAtendente").value = ""
    let respostaNotaProduto = document.getElementById("idRangeProduto").value = ""
    let respostaIdadeEntrevistado = document.getElementById("idIdadeEntrevistado").value = ""
    var respostaGeneroEntrevistado = document.querySelector("input[name=nmRadioGenero]:checked").value = ""
}