


window.onload = async function () {
    let dados = await buscarRelatorio()

    if (dados == "Usuário não autorizado") {
        window.location.href = "http://localhost:3001/login" 
    } else {
        let divDados = document.getElementById("idDados")

        //respostas pergunta A
        let perguntaA = document.createElement("h2")
        perguntaA.className = "bg-info text-white rounded text-center"
        perguntaA.innerHTML = "Sua solicitação foi atendida?"

        let naoAtendida = document.createElement("p")
        naoAtendida.innerHTML = `Não atendida: ${dados.a.naoFoiAtendida} respostas`

        let parcialmenteAtendida = document.createElement("p")
        parcialmenteAtendida.innerHTML = `Parcialmente atendida: ${dados.a.parcialmenteAtendida} respostas`

        let totalmenteAtendida = document.createElement("p")
        totalmenteAtendida.innerHTML = `Totalmente Atendida: ${dados.a.totalmenteAtendida} respostas`


        //respostas Pergunta B
        let perguntaB = document.createElement("h2")
        perguntaB.className = "bg-info text-white rounded text-center"
        perguntaB.innerHTML = "Qual nota você daria par ao atendimento"

        let aceitavel = document.createElement("p")
        aceitavel.innerHTML = `Aceitavel: ${dados.b.aceitavel} respostas`

        let bom = document.createElement("p")
        bom.innerHTML = `Bom: ${dados.b.bom} respostas`

        let excelente = document.createElement("p")
        excelente.innerHTML = `Excelente: ${dados.b.excelente} respostas`

        let pessimo = document.createElement("p")
        pessimo.innerHTML = `Péssimo: ${dados.b.pessimo} respostas`

        let ruim = document.createElement("p")
        ruim.innerHTML = `Ruim: ${dados.b.ruim} respostas`


        //respostas perguntaC

        let perguntaC = document.createElement("h2")
        perguntaC.className = "bg-info text-white rounded text-center"
        perguntaC.innerHTML = "Como você classificaria o comportamento do atendente?"

        let educado = document.createElement("p")
        educado.innerHTML = `Educado: ${dados.c.educado} respostas`

        let indelicado = document.createElement("p")
        indelicado.innerHTML = `Indelicado: ${dados.c.indelicado} respostas`

        let mauHumorado = document.createElement("p")
        mauHumorado.innerHTML = `Mau Humorado: ${dados.c.mauHumorado} respostas`

        let muitoAtencioso = document.createElement("p")
        muitoAtencioso.innerHTML = `Muito Atencioso: ${dados.c.muitoAtencioso} respostas`

        let neutro = document.createElement("p")
        neutro.innerHTML = `Neutro: ${dados.c.neutro} respostas`

        // //respostasPergunta D
        let perguntaD = document.createElement("h2")
        perguntaD.className = "bg-info text-white text-center"
        perguntaD.innerHTML = "Qual nota você daria para o produto?"

        let zero = document.createElement("p")
        zero.innerHTML = `Zero: ${dados.d[0]} respostas`

        let um = document.createElement("p")
        um.innerHTML = `Um: ${dados.d[1]} respostas`

        let dois = document.createElement("p")
        dois.innerHTML = `Dois: ${dados.d[2]} respostas`

        let tres = document.createElement("p")
        tres.innerHTML = `Três: ${dados.d[3]} respostas`

        let quatro = document.createElement("p")
        quatro.innerHTML = `Quatro: ${dados.d[4]} respostas`

        let cinco = document.createElement("p")
        cinco.innerHTML = `Cinco: ${dados.d[5]} respostas`

        let seis = document.createElement("p")
        seis.innerHTML = `Seis: ${dados.d[6]} respostas`

        let sete = document.createElement("p")
        sete.innerHTML = `Sete: ${dados.d[7]} respostas`

        let oito = document.createElement("p")
        oito.innerHTML = `Oito: ${dados.d[8]} respostas`

        let nove = document.createElement("p")
        nove.innerHTML = `Nove: ${dados.d[9]} respostas`

        let dez = document.createElement("p")
        dez.innerHTML = `Dez: ${dados.d[10]} respostas`


        //respostas pergunta E
        let perguntaE = document.createElement("h2")
        perguntaE.className = "bg-info text-white rounded text-center"
        perguntaE.innerHTML = "Informe sua idade"

        let acimade50 = document.createElement("p")
        acimade50.innerHTML = `Acima de 50: ${dados.e.acimade50} respostas`

        let entre15e21 = document.createElement("p")
        entre15e21.innerHTML = `Entre 15 e 21 anos: ${dados.e.entre15e21} respostas`

        let entre22e35 = document.createElement("p")
        entre22e35.innerHTML = `Entre 22 e 35 anos: ${dados.e.entre22e35} respostas`

        let entre36e50 = document.createElement("p")
        entre36e50.innerHTML = `Entre 36 e 50 anos: ${dados.e.entre36e50} respostas`

        let menorQueQuinze = document.createElement("p")
        menorQueQuinze.innerHTML = `Menor que 15: ${dados.e.menorQueQuinze} respostas`


        //respostas pergunta F
        let perguntaF = document.createElement("h2")
        perguntaF.className = "bg-info text-white rounded text-center"
        perguntaF.innerHTML = "Informe seu gênero:"

        let feminino = document.createElement("p")
        feminino.innerHTML = `Feminino: ${dados.f.feminino} respostas`

        let lgbt = document.createElement("p")
        lgbt.innerHTML = `LGBT: ${dados.f.lgbt} respostas`

        let masculino = document.createElement("p")
        masculino.innerHTML = `Masculino: ${dados.f.masculino} respostas`


        divDados.appendChild(perguntaA)
        divDados.appendChild(naoAtendida)
        divDados.appendChild(parcialmenteAtendida)
        divDados.appendChild(totalmenteAtendida)
        divDados.appendChild(perguntaB)
        divDados.appendChild(aceitavel)
        divDados.appendChild(bom)
        divDados.appendChild(excelente)
        divDados.appendChild(pessimo)
        divDados.appendChild(ruim)
        divDados.appendChild(educado)
        divDados.appendChild(indelicado)
        divDados.appendChild(mauHumorado)
        divDados.appendChild(muitoAtencioso)
        divDados.appendChild(neutro)
        divDados.appendChild(perguntaD)
        divDados.appendChild(zero)
        divDados.appendChild(dois)
        divDados.appendChild(tres)
        divDados.appendChild(quatro)
        divDados.appendChild(cinco)
        divDados.appendChild(seis)
        divDados.appendChild(sete)
        divDados.appendChild(oito)
        divDados.appendChild(nove)
        divDados.appendChild(dez)
        divDados.appendChild(perguntaE)
        divDados.appendChild(acimade50)
        divDados.appendChild(entre15e21)
        divDados.appendChild(entre22e35)
        divDados.appendChild(entre36e50)
        divDados.appendChild(menorQueQuinze)
        divDados.appendChild(perguntaF)
        divDados.appendChild(masculino)
        divDados.appendChild(feminino)
        divDados.appendChild(lgbt)
    }
}

async function buscarRelatorio() {

    let autoriza = localStorage.getItem("token");

    const options = {
        method: "GET",
        headers: {
            "x-access-token": autoriza,
        },
    }

    let dados = await fetch("http://localhost:3001/estatisticas", options)

    if (dados.status == 401) {
        return dados = "Usuário não autorizado"
    }
    return dados.json()
}