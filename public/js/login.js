

var btnEntrar = document.getElementById("idBtnEntrar")

btnEntrar.onclick = async function () {   
    let usuario = document.getElementById("idUsuario").value
    let senha = document.getElementById("idSenha").value
    
    let dadosUsuario = {
        userId: usuario,
        pass: senha
    }
    let logar = await verificaUsuario(dadosUsuario)

    if (logar.auth == true) {
        localStorage.setItem('token', logar.token);
        window.location.href = "http://localhost:3001/relatorio" 
        
    }if (logar == "Usuario não autorizado") {
        let div = document.createElement("div")
        div.style.height = "10px"
        div.className = "alert alert-danger d-flex align-items-center justify-content-center"
        let alerta = document.getElementById("idAlerta")
        let textoAlerta = document.createElement("h2")
        textoAlerta.innerText = "Usuário e/ou senha inválidos"
        textoAlerta.className = "text-dange fs-6"
        setTimeout(() => {
            alerta.innerText = ""
           
        }, 1000);

        div.appendChild(textoAlerta)
        alerta.appendChild(div)
    }    
}

async function verificaUsuario(dadosUsuario) {

    const options = {
        method: "POST",
        headers: {
            'Content-type': 'application/json',
        },
        body: JSON.stringify(dadosUsuario)
    }    
    let verificaUsuario = await fetch("http://localhost:3001/login", options)
    if (verificaUsuario.status != 200) {
        return "Usuario não autorizado"
    }
    return verificaUsuario.json()
}