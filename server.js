const express = require("express")
const path = require("path")
const app = express()
const fs = require("fs")
const cors = require("cors")
const jwt = require("jsonwebtoken")
const SECRET = "EuTbSei"
const porta = 3001

app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "public/js")));
app.use(express.static(path.join(__dirname, "public/img")));
app.use(express.static(path.join(__dirname, "public/css")));
app.use(express.static(path.join(__dirname, "view")));

app.listen(porta, function () {
    console.log(`Servidor rodando na porta ${porta}`)
})

app.get("/", function (req, resp) {

    resp.sendFile(path.join(__dirname + "/view/index.html"))

})

app.post("/pesquisa", function (req, resp) {

    var dataResposta = new Date().getTime()
    let mensagem

    let dados = `${req.body.respostaSolicitacao},${req.body.respostaNotaAtendimento},${req.body.respostaNotaAtendente},${req.body.respostaNotaProduto},${req.body.respostaIdadeEntrevistado},${req.body.respostaGeneroEntrevistado},${dataResposta};`

    fs.appendFile("pesquisa.csv", dados, function (err) {
        if (err) {
            mensagem = {
                "status": "500",
                mensagem: err
            }
            resp.send(mensagem)
        } else {
            mensagem = {
                "status": "200",
                mensagem: "Pesquisa Registrada Com sucesso"
            }
            resp.send(mensagem)
        }
    })
})

app.get("/relatorio", function (req, resp) {

    resp.sendFile(path.join(__dirname + "/view/relatorio.html"))

})

app.get("/estatisticas", verificaUser, async function (req, resp) {
    let dados = await gerarRelatorio()

    resp.send(dados)
})


function verificaUser(req, resp, next) {

    const token = req.header("x-access-token")
    jwt.verify(token, SECRET, function (err, decoded) {
        if (err) {
            return resp.status(401).end()
        }
        req.dec = decoded.xxx
        next()
    })
}

app.get("/login", function (req, resp) {
    resp.sendFile(path.join(__dirname, "/view/login.html"))
})

app.get("/usuario", verificaUser, function (req, resp) {

    resp.json({
        msn: "Chegou na rota usuario - GET",
        userId: req.query.userId,
        pass: req.query.pass
    })
})

app.post("/login", function (req, resp) {

    fs.readFile("usuarios.csv", "utf8", function (err, dados) {
        let usuarios = []
        let arrayUsuarios = dados.split(";")
        let usuario = []

        arrayUsuarios.forEach(function (element) {
            usuarios.push(element.split(","))
        })

        for (let index = 0; index < usuarios.length; index++) {
            if (usuarios[index][0] == req.body.userId) {
                usuario.push(usuarios[index][0])
                usuario.push(usuarios[index][1])
            }
        }
        if (req.body.userId == usuario[0] && req.body.pass === usuario[1]) {
            const token = jwt.sign({ xxx: req.body.userId }, SECRET, { expiresIn: 20 })
            return resp.json({ auth: true, token })
        }
        resp.status(401).end()
    })
})

function gerarRelatorio() {

    return new Promise(function (resolve, reject) {
        fs.readFile("pesquisa.csv", "utf8", function (err, dados) {

            var respostas = []

            separarespostas = dados.split(";")
            separarespostas.forEach(function (element) {
                respostas.push(element.split(","))
            })
            for (let index = 0; index < respostas.length; index++) {
                if (respostas[index] == "") {
                    respostas.splice(index, 1)
                }
            }

            let respostasPerguntaA = []
            let naoAtendida = ["Não foi atendida", 0]
            let parcialmenteAtendida = ["Parcialmente atendida", 0]
            let totalmenteAtendida = ["Totalmente atendida", 0]

            for (let index = 0; index < respostas.length; index++) {
                respostasPerguntaA.push(respostas[index][0])
            }
            for (let index = 0; index < respostasPerguntaA.length; index++) {
                if (respostasPerguntaA[index] == "Não foi atendida") {
                    naoAtendida[1]++
                } if (respostasPerguntaA[index] == "Parcialmente atendida") {
                    parcialmenteAtendida[1]++
                } if (respostasPerguntaA[index] == "Totalmente atendida") {
                    totalmenteAtendida[1]++
                }
            }

            let respostasPerguntaB = []
            let pessimo = ["Péssimo", 0]
            let ruim = ["Ruim", 0]
            let aceitavel = ["Aceitável", 0]
            let bom = ["Bom", 0]
            let excelente = ["Excelente", 0]

            for (let index = 0; index < respostas.length; index++) {
                respostasPerguntaB.push(respostas[index][1])
            }

            for (let index = 0; index < respostasPerguntaB.length; index++) {
                if (respostasPerguntaB[index] == "Péssimo") {
                    pessimo[1]++
                } if (respostasPerguntaB[index] == "Ruim") {
                    ruim[1]++
                } if (respostasPerguntaB[index] == "Aceitavel") {
                    aceitavel[1]++
                } if (respostasPerguntaB[index] == "Bom") {
                    bom[1]++
                } if (respostasPerguntaB[index] == "Excelente") {
                    excelente[1]++
                }
            }

            let respostasPerguntaC = []
            let indelicado = ["Indelicado", 0]
            let mauHumorado = ["Mau humorado", 0]
            let neutro = ["Neutro", 0]
            let educado = ["Educado", 0]
            let muitoAtencioso = ["Muito Atencioso", 0]

            for (let index = 0; index < respostas.length; index++) {
                respostasPerguntaC.push(respostas[index][2])
            }

            for (let index = 0; index < respostasPerguntaC.length; index++) {
                if (respostasPerguntaC[index] == "Indelicado") {
                    indelicado[1]++
                } if (respostasPerguntaC[index] == "Mau humorado") {
                    mauHumorado[1]++
                } if (respostasPerguntaC[index] == "Neutro") {
                    neutro[1]++
                } if (respostasPerguntaC[index] == "Educado") {
                    educado[1]++
                } if (respostasPerguntaC[index] == "Muito Atencioso") {
                    muitoAtencioso[1]++
                }
            }

            let respostasPerguntaD = []
            let notaZero = 0
            let notaUm = 0
            let notaDois = 0
            let notaTres = 0
            let notaQuatro = 0
            let notaCinco = 0
            let notaSeis = 0
            let notaSete = 0
            let notaOito = 0
            let notaNove = 0
            let notaDez = 0

            for (let index = 0; index < respostas.length; index++) {
                respostasPerguntaD.push(respostas[index][3])
            }

            for (let index = 0; index < respostasPerguntaD.length; index++) {
                if (respostasPerguntaD[index] == "0") {
                    notaZero++
                } if (respostasPerguntaD[index] == "1") {
                    notaUm++
                } if (respostasPerguntaD[index] == "2") {
                    notaDois++
                } if (respostasPerguntaD[index] == "3") {
                    notaTres++
                } if (respostasPerguntaD[index] == "4") {
                    notaQuatro++
                } if (respostasPerguntaD[index] == "5") {
                    notaCinco++
                } if (respostasPerguntaD[index] == "6") {
                    notaSeis++
                } if (respostasPerguntaD[index] == "7") {
                    notaSete++
                } if (respostasPerguntaD[index] == "8") {
                    notaOito++
                } if (respostasPerguntaD[index] == "9") {
                    notaNove++
                } if (respostasPerguntaD[index] == "10") {
                    notaDez++
                }
            }

            let respostasPerguntaE = []
            let menorDeQuinze = 0
            let quinzeAVinteUm = 0
            let vinteDoisATrintaCinco = 0
            let trintaSeisACinquenta = 0
            let acimaCinquenta = 0

            for (let index = 0; index < respostas.length; index++) {
                respostasPerguntaE.push(respostas[index][4])
            }

            for (let index = 0; index < respostasPerguntaE.length; index++) {
                if (respostasPerguntaE[index] < "15") {
                    menorDeQuinze++
                } if (respostasPerguntaE[index] >= "15" && respostasPerguntaE[index] <= "21") {
                    quinzeAVinteUm++
                } if (respostasPerguntaE[index] >= "22" && respostasPerguntaE[index] <= "35") {
                    vinteDoisATrintaCinco++
                } if (respostasPerguntaE[index] >= "36" && respostasPerguntaE[index] <= "50") {
                    trintaSeisACinquenta++
                } if (respostasPerguntaE[index] > "50") {
                    acimaCinquenta++
                }
            }

            let respostasPerguntaF = []
            let masculino = 0
            let feminino = 0
            let lgbt = 0

            for (let index = 0; index < respostas.length; index++) {
                respostasPerguntaF.push(respostas[index][5])
            }

            for (let index = 0; index < respostasPerguntaF.length; index++) {
                if (respostasPerguntaF[index] == "Masculino") {
                    masculino++
                } if (respostasPerguntaF[index] == "Feminino") {
                    feminino++
                } if (respostasPerguntaF[index] == "LGBT") {
                    lgbt++
                }
            }

            let dadoss = ({
                "a": {
                    "naoFoiAtendida": naoAtendida[1],
                    "parcialmenteAtendida": parcialmenteAtendida[1],
                    "totalmenteAtendida": totalmenteAtendida[1]
                },
                "b": {
                    "pessimo": pessimo[1],
                    "ruim": ruim[1],
                    "aceitavel": aceitavel[1],
                    "bom": bom[1],
                    "excelente": excelente[1]
                },
                "c": {
                    "indelicado": indelicado[1],
                    "mauHumorado": mauHumorado[1],
                    "neutro": neutro[1],
                    "educado": educado[1],
                    "muitoAtencioso": muitoAtencioso[1]
                },
                "d": {
                    "0": notaZero,
                    "1": notaUm,
                    "2": notaDois,
                    "3": notaTres,
                    "4": notaQuatro,
                    "5": notaCinco,
                    "6": notaSeis,
                    "7": notaSete,
                    "8": notaOito,
                    "9": notaNove,
                    "10": notaDez
                },
                "e": {
                    "menorQueQuinze": menorDeQuinze,
                    "entre15e21": quinzeAVinteUm,
                    "entre22e35": vinteDoisATrintaCinco,
                    "entre36e50": trintaSeisACinquenta,
                    "acimade50": acimaCinquenta
                },
                "f": {
                    "masculino": masculino,
                    "feminino": feminino,
                    "lgbt": lgbt
                }
            })
            resolve(dadoss)
        })
    })
}
