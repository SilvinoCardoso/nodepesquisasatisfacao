# Projeto formulário de pesquisa empresa XYZ

## Exercício 04 

> A empresa XYZ quer exibir um formulário de pesquisa de satisfação ao termino de seu atendimento online. Crie uma tela com perguntas e respostas do tipo multipla-escolha:
a. Sua solicitação foi atendida? (não foi atendida, parcialmente atendida, totalmente atendida)
b. Qual nota você daria para o atendimento? (péssimo, ruim, aceitável, bom, excelente)
c. Como você classificaria o comportamento do atendente? (Indelicado, Mau humorado, Neutro, Educado, Muito Atencioso)
d. De 0 à 10, qual nota você daria para o produto (utilize range)
e. Informe Sua idade (campo numérico)
f. Informa seu Gênero (Masculino/Feminino/LGBT)
